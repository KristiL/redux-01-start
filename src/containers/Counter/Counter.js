import React, { Component } from 'react';
import { connect } from 'react-redux'; // connect is a function, a higher order component

import CounterControl from '../../components/CounterControl/CounterControl';
import CounterOutput from '../../components/CounterOutput/CounterOutput';

class Counter extends Component {
    // state = {
    //     counter: 0
    // }

    // counterChangedHandler = ( action, value ) => {
    //     switch ( action ) {
    //         case 'inc':
    //             this.setState( ( prevState ) => { return { counter: prevState.counter + 1 } } )
    //             break;
    //         case 'dec':
    //             this.setState( ( prevState ) => { return { counter: prevState.counter - 1 } } )
    //             break;
    //         case 'add':
    //             this.setState( ( prevState ) => { return { counter: prevState.counter + value } } )
    //             break;
    //         case 'sub':
    //             this.setState( ( prevState ) => { return { counter: prevState.counter - value } } )
    //             break;
    //     }
    // }

    render () {
        return (
            <div>
                <CounterOutput value={this.props.ctr} />
                <CounterControl label="Increment" clicked={this.props.onIncrementCounter} />
                <CounterControl label="Decrement" clicked={this.props.onDecrementCounter}  />
                <CounterControl label="Add 10" clicked={this.props.onAddCounter}  />
                <CounterControl label="Subtract 8" clicked={this.props.onSubtractCounter}  />
            </div>
        );
    }
}

// function             redux state
const mapStateToProps = state => {
    return {
        ctr: state.counter // muutujale annan suva nime, nt siin pandud ctr. seda saan siis siin sees kasutada
    };
}

const mapDispatchToProps = dispatch => //which kind of actions do I want to dispacth in this container
// also stores a function which will receive dispatch
// helper function which will call dispatch on the store behind the scenes
{
    return { // JS object; define property names which will hold a reference to a function
        // which will eventually get executed to dispatch an action
        onIncrementCounter: () => dispatch({type: 'INCREMENT'}), //anonymous function. will be executed
        // kui koodis funktsioon ilma () siis ei kutsuta kohe välja vaid alles kui vajutatakse, lihtsalt reference
        onDecrementCounter: () => dispatch({type: 'DECREMENT'}),
        onAddCounter: () => dispatch({type: 'ADD', value: 10}),
        onSubtractCounter: () => dispatch({type: 'SUBTRACT', value: 8})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter); // connect() <- siia sulgudesse configuration:
    // which part of the whole application state is interesting to us, which slice do i want to get for this container,
    // which actions do i want to dispatch
    // kui ei ole ühte kahest argumendist vaja: esimene asendada null-ga, teise võib lihtsalt ära jätta