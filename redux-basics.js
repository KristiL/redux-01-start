const redux = require('redux'); // see on node.js süntaks. muidu import lausega reactis
const createStore = redux.createStore; // see on funktsioon. ei jooksuta kohe. sellega saab luua uue redux store-i

// ise valin nime, nagu kõigi const puhul.
// võib olla nr või objekt või misiganes mul stateis olema peab
const initialState = {
   counter: 0
}

// meil on 1 reducer võimalik kasutada. isegi kui teeme mitu, need Merge'itakse üheks kokku.
// reducer on ainus asi, mis saab uuendada statei pärast. sellepärast teen enne reduceri ja panen selle storei sisse
// Reducer
// nime valin ise. see on funktsioon.
// kaks argumenti: hetke state ja teine on tegevus. funktsioon tagastab uuendatud statei.

// const rootReducer = (state, action) => {
//    return state; // sellisel kujul tagastatakse sama algne state
// };

// state = initialState <- default value määramine state'ile, ehk kui state on undefined siis kasutab antud juhul initialState väärtust (ES6)
// siis esimesel korral kasutatakse initialStatei. edaspidi ongi see väärtus juba state.
const rootReducer = (state = initialState, action) => {
//   return state; // sellisel kujul tagastatakse sama algne state
   if (action.type === 'INC_COUNTER') {
      return { // always do this IMMUTABLY! NEVER mutate any data
         ...state, // copy previous state, properties and values
         counter: state.counter + 1   // if that also would be a JS object, you would have to copy it first, too
         // state.counter: ainult loen eelmise statei väärtust. pärast uue väärtuse salvestan counter propertisse.
      }
   }
   if (action.type === 'ADD_COUNTER') {
      return {
         ...state,
         counter: state.counter + action.value
      }
   }
   return state;
};

// Store
const store = createStore(rootReducer); // sellele store'ile võib anda suvalise oma valitud nime
// store tuleb initsialiseerida reduceriga.
// algul tühi state ehk undefined
console.log(store.getState());

// Subscription
store.subscribe(() => // takes an argument, a function that will be executed whenever the state is updated
   // e.g. whenever an action reached through reducer
{ // in the function body we can execute any code we want on state updates
   console.log('[Subscription]', store.getState());
});
// kuigi subscriptioni kirjutasime viimasena, siis see peaks siin kuulama enne igat actionit, et tegutseda nii kui
   // mingi muudatus on tehtud

// Dispatching Action
store.dispatch({type: 'INC_COUNTER'}); // dispatch on funktsioon mis võtab argumendina "action"i
// action peab olema JS object, millel peab olema type property
// pärast seda type'i kasutatakse, et which type of action was dispatched ja mida reduceris teha tuleb?
// type nime valin ise, convention on kasutada "all uppercase string-i" (sõnade vahe: _)
store.dispatch({type: 'ADD_COUNTER', value: 10});
// järgmiste argumentidena ehk "(data) payload" võib anda mitu asja või nt 1 asja, mis võib omakorda olla JS object ja
   // grupeerida kogu data, mida tahan edastada
console.log(store.getState());


